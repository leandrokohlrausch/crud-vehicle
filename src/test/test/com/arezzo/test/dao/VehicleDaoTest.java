package test.com.arezzo.test.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.arezzo.test.config.SpringMVCConfiguration;
import com.arezzo.test.model.Vehicle;
import com.arezzo.test.service.VehiclesService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { SpringMVCConfiguration.class})

public class VehicleDaoTest {
	
	@Autowired
	private VehiclesService vehiclesService;
	
	@Test
	public void list() {
		List<Vehicle> list = vehiclesService.list();
		Assert.assertNotNull(list);
	}
	
	@Test
	public void get(){
		Vehicle vehicle = this.getOneVehicle();
		Long id = vehicle.getId();
		Vehicle v = vehiclesService.get(id);
		Assert.assertNotNull(v);
		Assert.assertEquals(v.getId(), vehicle.getId());
		Assert.assertEquals(v.getManufacturer(), vehicle.getManufacturer());
		Assert.assertEquals(v.getModel(), vehicle.getModel());
		Assert.assertEquals(v.getYear(), vehicle.getYear());
	}
	
	@Test
	public void delete(){
		Vehicle vehicle = this.getOneVehicle();
		Long id = vehicle.getId();
		vehiclesService.delete(id);
		Vehicle v = vehiclesService.get(id);
		Assert.assertNull(v);
	}
	
	@Test
	public void save(){
		Vehicle vehicle = new Vehicle();
		String model = "CORVETTE";
		String manufacturer = "CHEVROLET";
		Integer year = 1967;
		vehicle.setManufacturer(manufacturer);
		vehicle.setModel(model);
		vehicle.setYear(year);
		vehiclesService.save(vehicle);
		List<Vehicle> list = vehiclesService.list();
		Vehicle v = null;
		for (Vehicle it : list) {
			if (it.getModel().equals(model) && it.getManufacturer().equals(manufacturer) && it.getYear().equals(year)) {
				v = it;
				break;
			}
		}
		Assert.assertNotNull(v);
	}
	
	@Test
	public void update(){
		Vehicle vehicle = this.getOneVehicle();
		Long id = vehicle.getId();
		String model = "VECTRA GT";
		String manufacturer = "CHEVROLET";
		Integer year = 2099;
		Vehicle v = new Vehicle();
		v.setManufacturer(manufacturer);
		v.setModel(model);
		v.setYear(year);
		v.setId(id);
		vehiclesService.update(v);
		v = vehiclesService.get(id);
		Assert.assertNotNull(v);
		Assert.assertEquals(v.getId(), id);
		Assert.assertEquals(v.getModel(), model);
		Assert.assertEquals(v.getManufacturer(), manufacturer);
		Assert.assertEquals(v.getYear(), year);
	}
	
	private Vehicle getOneVehicle(){
		List<Vehicle> vehicles = vehiclesService.list();
		if (vehicles == null || vehicles.isEmpty()) {
			Vehicle vehicle = new Vehicle();
			vehicle.setManufacturer("FORD");
			vehicle.setModel("FIESTA");
			vehicle.setYear(2012);
			vehiclesService.save(vehicle);
			vehicles = vehiclesService.list();
		}
		return vehicles.get(0);
	}
	
}
