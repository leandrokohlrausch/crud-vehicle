package test.com.arezzo.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.arezzo.test.config.SpringMVCConfiguration;
import com.arezzo.test.model.Vehicle;
import com.arezzo.test.service.VehiclesService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { SpringMVCConfiguration.class})

public class VehiclesControllerTest {

	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	private VehiclesService vehiclesService;
	
	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}
	
	@Test
	public void vehiclesList() throws Exception {
		this.vehiclesGenericTest("list", "vehicles", null, "list");
	}
	
	@Test
	public void vehiclesCreate() throws Exception {
		this.vehiclesGenericTest("create", "vehicle", null, "create");
	}
	
	@Test
	public void vehiclesEdit() throws Exception {
		this.vehiclesGenericTest("edit", "vehicle",  this.getIdOneVehicle(), "edit");
	}
	
	@Test
	public void vehiclesShow() throws Exception {
		this.vehiclesGenericTest("show", "vehicle",  this.getIdOneVehicle(), "show");
	}
	
	private Long getIdOneVehicle(){
		List<Vehicle> vehicles = vehiclesService.list();
		if (vehicles == null || vehicles.isEmpty()) {
			Vehicle vehicle = new Vehicle();
			vehicle.setManufacturer("FORD");
			vehicle.setModel("FIESTA");
			vehicle.setYear(2012);
			vehiclesService.save(vehicle);
			vehicles = vehiclesService.list();
		}
		return vehicles.get(0).getId();
	}
	
	
	private void vehiclesGenericTest(String action, String attribute, Long id, String redirect) throws Exception{
		if (id != null) {
			this.mockMvc.perform(get("/vehicles/"+ action, id))
			.andExpect(status().isOk())
			.andExpect(model().attributeExists(attribute))
			.andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/vehicles/"+redirect+".jsp"));
		} else {		
			this.mockMvc.perform(get("/vehicles/"+ action))
			.andExpect(status().isOk())
			.andExpect(view().name("vehicles/"+ action))
			.andExpect(model().attributeExists(attribute))
			.andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/vehicles/"+redirect+".jsp"));		
		}
	}
}
