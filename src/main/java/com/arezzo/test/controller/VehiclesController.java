package com.arezzo.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.arezzo.test.model.Vehicle;
import com.arezzo.test.service.VehiclesService;
import com.arezzo.test.validator.VehicleValidator;

@Controller
@RequestMapping(value="/vehicles")
public class VehiclesController {
	
	@Autowired
	VehiclesService vehiclesService;
	
	@InitBinder
	protected void initBinder(WebDataBinder webDataBinder) {
		webDataBinder.setValidator(new VehicleValidator());
	}
	
	@RequestMapping(value={"/", "/list"}, method=RequestMethod.GET)
	public ModelAndView list(RedirectAttributes redirectAttributes){
		List<Vehicle> vehicles = vehiclesService.list();
		ModelAndView model = new ModelAndView("/vehicles/list");
		model.addObject("vehicles", vehicles);	
		return model;
	}
	
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView create(Vehicle vehicle){
		ModelAndView model = new ModelAndView("/vehicles/create");
		vehicle = vehicle != null ? vehicle :new Vehicle();
		model.addObject("vehicle", vehicle);
		return model;
	}
	
	@RequestMapping(method=RequestMethod.POST , name="saveVehicle")
	public ModelAndView save(@RequestParam("photo") MultipartFile photo, @Validated Vehicle vehicle, BindingResult bindingResult, RedirectAttributes redirectAttributes){
		if (bindingResult.hasErrors()) {
			ModelAndView model = new ModelAndView("redirect:/vehicles/create");
			model.addObject("vehicle", vehicle);
			return model;
		}
		
		vehicle.setPhotoPath(vehiclesService.writePhoto(photo));
		Boolean saved = vehiclesService.save(vehicle);
		
		if (saved) {
			redirectAttributes.addFlashAttribute("message", "Criado com sucesso");
		} else {
			redirectAttributes.addFlashAttribute("message", "Falha ao criar");
		}
		
		return new ModelAndView("redirect:/vehicles/list");	
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable("id") Long id){
		ModelAndView model = new ModelAndView("/vehicles/edit");
		Vehicle vehicle = vehiclesService.get(id);
		model.addObject("vehicle", vehicle);
		return model;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST, name="updateVehicle")
	public ModelAndView update(@RequestParam("photo") MultipartFile photo,@Validated Vehicle vehicle, BindingResult bindingResult, RedirectAttributes redirectAttributes){
		Vehicle v = vehiclesService.get(vehicle.getId());
		
		if (bindingResult.hasErrors() || v == null) {
			return new ModelAndView("redirect:/vehicles/edit/" + vehicle.getId());
		}
		vehicle.setPhotoPath(v.getPhotoPath());
		vehicle.setPhotoPath(vehiclesService.updatePhoto(vehicle, photo));
		Boolean saved = vehiclesService.update(vehicle);
		if (saved) {
			redirectAttributes.addFlashAttribute("message", "Atualizado com Sucesso");
		} else {
			redirectAttributes.addFlashAttribute("message", "Falha ao Atualizar");
		}
		return new ModelAndView("redirect:/vehicles/list");
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public ModelAndView show(@PathVariable("id") Long id){
		ModelAndView model = new ModelAndView("/vehicles/show");
		Vehicle vehicle = vehiclesService.get(id);
		model.addObject("vehicle", vehicle);
		return model;
	}
		
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
		Boolean saved = vehiclesService.delete(id);
		
		if (saved) {
			redirectAttributes.addFlashAttribute("message", "Excluído com Sucesso");
		} else {
			redirectAttributes.addFlashAttribute("message", "Falha ao Excluir");
		}
		
		return new ModelAndView("redirect:/vehicles/list");
	}
}
