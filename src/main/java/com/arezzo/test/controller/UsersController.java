package com.arezzo.test.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.arezzo.test.model.User;
import com.arezzo.test.service.UsersService;

@Controller
@RequestMapping(value="/users")
public class UsersController {
	
	@Autowired
	private UsersService usersService;
	
	public static final String USER_SESSION = "___user_SESSION__";
	
	@RequestMapping(value="login", method=RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView model = new ModelAndView("/users/login");
		model.addObject("user", new User());
		return model;
	}
	
	@RequestMapping(value="authenticate", method=RequestMethod.POST, name="authenticate")
	public ModelAndView authenticate(User user, HttpSession httpSession, RedirectAttributes redirectAttributes){
		user.setEmail(user.getUsername().contains("@") ? user.getUsername() : null);
		Boolean auth = usersService.authenticationUser(user);
		String redirect = "redirect:/vehicles/list";
		if (!auth) {
			redirectAttributes.addFlashAttribute("message", "Dados Inválidos para o Login");
			redirect = "redirect:/users/login";
		} else {
			httpSession.setAttribute(USER_SESSION, user);
		}
		return new ModelAndView(redirect);
	}
	
	@RequestMapping(value="logout", method=RequestMethod.GET)
	public String logout(HttpSession session) {
	  session.invalidate();
	  return "redirect:/users/login";
	}

}
