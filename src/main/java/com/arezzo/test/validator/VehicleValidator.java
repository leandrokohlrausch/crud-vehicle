package com.arezzo.test.validator;

import java.util.Calendar;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.arezzo.test.model.Vehicle;

public class VehicleValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Vehicle.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "model", "field.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "manufacturer", "field.required");
		Vehicle vehicle = (Vehicle) target;
		Calendar cal = Calendar.getInstance();
		Integer currentYear = cal.get(Calendar.YEAR);
		if (vehicle.getYear() > currentYear) {
			errors.rejectValue("year", "field.value.invalid");
		}
	}

}
