package com.arezzo.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="vehicles")
public class Vehicle {

	@Id
	@Column(name="id") 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vehicles_seq_gen")
	@SequenceGenerator(name = "vehicles_seq_gen", sequenceName = "vehicles_id_seq")
	private Long id;
	
	@Column(name="model")
	private String model;
	
	@Column(name="manufacturer")
	private String manufacturer;
	
	@Column(name="year")
	private Integer year;
	
	@Column(name="photo_path")
	private String photoPath;
	
	@Transient
	private String encodeBase64Photo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getEncodeBase64Photo() {
		return encodeBase64Photo;
	}

	public void setEncodeBase64Photo(String encodeBase64Photo) {
		this.encodeBase64Photo = encodeBase64Photo;
	}
}
