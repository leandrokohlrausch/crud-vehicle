package com.arezzo.test.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.arezzo.test.controller.HomeController;
import com.arezzo.test.controller.UsersController;


@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller) throws Exception {
		HandlerMethod handler = (HandlerMethod) controller;
		if (handler.getBean() instanceof HomeController || handler.getBean() instanceof UsersController) {
			return true;
		}

		if (request.getSession().getAttribute(UsersController.USER_SESSION) != null) {
			return true;
		}

		response.sendRedirect(request.getContextPath() + "/users/login");
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)	throws Exception {
	}
}
