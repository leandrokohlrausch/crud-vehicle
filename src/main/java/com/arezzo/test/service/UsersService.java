package com.arezzo.test.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arezzo.test.dao.UserDao;
import com.arezzo.test.model.User;

@Service
@Transactional
public class UsersService {
	
	@Autowired
	private UserDao userDao;
	
	public Boolean authenticationUser(User user){
		String pass = user.getPassword();
		String username = user.getUsername();
		String email = user.getEmail();
		
		user = this.findUserByEmail(email);
		if (user == null) {
			user = this.findUserByUsername(username);
		}		
		return user != null ? user.getPassword().toUpperCase().equals(this.generateHashMd5(pass).toUpperCase()) : Boolean.FALSE;
	}
	
	private User findUserByEmail(String email){
		return userDao.findUserByEmail(email);
	}
	
	private User findUserByUsername(String username){
		return userDao.findUserByUsername(username);
	}
	
    private String generateHashMd5(String pass) {
        String md5 = "";
        if (pass != null && pass.length() > 0) {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");
                messageDigest.update(pass.getBytes("UTF-8"));
                byte byteData[] = messageDigest.digest();
                md5 = this.castByteArrayToString(byteData);
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        }
        return md5;
    }
    
    private String castByteArrayToString(byte[] byteData) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            stringBuffer.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return stringBuffer.toString();
    }    
}
