package com.arezzo.test.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.arezzo.test.dao.impl.VehicleDaoImpl;
import com.arezzo.test.model.Vehicle;

@Service
@Transactional
public class VehiclesService {

	@Autowired
	private VehicleDaoImpl vehicleDao;
	
	public Boolean save(Vehicle vehicle) {
		return vehicleDao.save(vehicle);
	}
	
	public Boolean update(Vehicle vehicle) {
		Vehicle vehicleUpdate = this.get(vehicle.getId());
		BeanUtils.copyProperties(vehicle, vehicleUpdate);
		return vehicleDao.update(vehicleUpdate);
	}
	
	public Boolean delete(Long id) {
		Vehicle vehicle = this.get(id);
		String path = vehicle != null ? vehicle.getPhotoPath() : null;
		Boolean deleted = vehicleDao.delete(id);
		if (path != null && deleted) {
			this.deletePhoto(path);
		}
		return deleted;
	}
	
	public List<Vehicle> list() {
		List<Vehicle> list =  vehicleDao.list();
		return list != null ? list : new ArrayList<Vehicle> ();
	}
	
	public Vehicle get(Long id) {
		Vehicle vehicle = null;
		if (id != null) {
			vehicle = vehicleDao.get(id);
			if (vehicle != null) {
				String b = "data:image/jpg;base64," + this.getEncodeBase64Img(vehicle.getPhotoPath());
				vehicle.setEncodeBase64Photo(b);
			}
		}
		return vehicle;
	}
	
	public String writePhoto(MultipartFile file){
		String path = System.getProperty("user.home");
		try {
			File folder = new File(path + "/.vehicles-crud/");
			if (!folder.exists()) {
				folder.mkdirs();
			}
			File photo = new File(folder.getAbsolutePath(), file.getOriginalFilename());
			file.transferTo(photo);
			return photo.getAbsolutePath();
		} catch(IOException e) {
			return null;
		}
	}
	
	public String updatePhoto(Vehicle vehicle, MultipartFile file) {
		String path = vehicle.getPhotoPath();
		if (file != null && file.getSize() > 0) {
			if (vehicle.getPhotoPath() != null) {
				File photo = new File(vehicle.getPhotoPath());
				if (photo.exists()) {
					photo.delete();
				}
			}
			path =  this.writePhoto(file);
		}		
		return path;
	}
	
	private void deletePhoto(String path){
		File photo = new File(path);
		if (photo.exists()) {
			photo.delete();
		}
	}
	
	public String getEncodeBase64Img(String path){
		if (path != null) {
			File img = new File(path);
			if (img.exists()) {
				try {
					byte[] binaryData = new byte[(int) img.length()];
					FileInputStream fileInputStream = new FileInputStream(img);
					fileInputStream.read(binaryData);
					fileInputStream.close();
					byte[] encoded = Base64.encodeBase64(binaryData);
					return new String(encoded);
				} catch (Exception e) {
					return null;
				}
			}
		}
		return null;
	}
	
}
