package com.arezzo.test.dao.impl;

import com.arezzo.test.dao.VehicleDao;
import com.arezzo.test.model.Vehicle;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VehicleDaoImpl implements VehicleDao {
	
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Boolean save(Vehicle vehicle) {
		Boolean saved  = Boolean.TRUE;
		try {
			this.getCurrentSession().save(vehicle);
		} catch (Exception ex) {
			saved = Boolean.FALSE;
		}
		return saved;
	}

	@Override
	public List<Vehicle> list() {
		Criteria criteria = this.getCurrentSession().createCriteria(Vehicle.class);
		return (List<Vehicle>) criteria.list();
	}

	@Override
	public Vehicle get(Long id){
		return (Vehicle) this.getCurrentSession().get(Vehicle.class, id);
	}

	@Override
	public Boolean delete(Long id) {
		Boolean saved  = Boolean.TRUE;
		try {
			this.getCurrentSession().delete(get(id));
		} catch (Exception ex) {
			saved = Boolean.FALSE;
		}
		return saved;
	}

	@Override
	public Boolean update(Vehicle vehicle) {
		Boolean saved  = Boolean.TRUE;
		try {
			this.getCurrentSession().update(vehicle);
		} catch (Exception ex) {
			saved = Boolean.FALSE;
		}
		return saved;
	}
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
}