package com.arezzo.test.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arezzo.test.dao.UserDao;
import com.arezzo.test.model.User;

@Repository
public class UserDaoImpl implements UserDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User findUserByUsername(String username) {
		return this.findUserByAttribute(username, "username");
	}

	@Override
	public User findUserByEmail(String email) {
		return this.findUserByAttribute(email, "email");
	}
	
	private User findUserByAttribute(String value, String parameter){
		if (value == null || parameter == null) {
			return null;
		}

		String hql ="SELECT u FROM User u WHERE u."+ parameter + " = :"+ parameter;
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(parameter, value);
		
		List<User> users = (List<User>) query.list();
		return users != null && !users.isEmpty()  ? users.get(0) : null;
	}
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

}
