package com.arezzo.test.dao;

import com.arezzo.test.model.User;

public interface UserDao {
	
	public User findUserByUsername(String username);
	
	public User findUserByEmail(String email);
}
