package com.arezzo.test.dao;

import java.util.List;

import com.arezzo.test.model.Vehicle;

public interface VehicleDao {

	public Boolean save(Vehicle vehicle);
	
	public List<Vehicle> list();
	
	public Vehicle get(Long id);
	
	public Boolean delete(Long id);
	
	public Boolean update(Vehicle vehicle);
}