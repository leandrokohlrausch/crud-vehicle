<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Login</title>
	</head>
	<body>
	
	<div>
		${message}
	</div>

	<h3>Login</h3>
	
	<form:form action="${spring:mvcUrl('authenticate').build()}" method="post" commandName="user">
		<div>
			<label for="username">Username or Email</label>
			<form:input path="username"/>
		</div>

		<div>
			<label for="password">Senha</label>
			<input type="password" name="password"/>
		</div>
		<div>
			<input type="submit" value="Login">
		</div>
	</form:form>
	
	</body>
</html>