<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Criar Ve�culo</title>
</head>
<body>	

	<a href="${pageContext.request.contextPath}/vehicles/"> Listagem de Ve�culos</a>
	<a href="${pageContext.request.contextPath}/users/logout"> Logout </a>
	
	<spring:hasBindErrors name="vehicle">
		<ul>
		<c:forEach var="error" items="${errors.allErrors}">	
			<li>${error.code}-${error.field}</li>
		</c:forEach>
		</ul>
	</spring:hasBindErrors>
	
	<form:form action="${spring:mvcUrl('saveVehicle').build()}" method="post" commandName="vehicle" enctype="multipart/form-data">
		<div>
			<label for="model">Modelo</label>
			<form:input path="model"/>
			<form:errors path="model"/>
		</div>
		<div>
			<label for="manufacturer">Fabricante</label>
			<form:input path="manufacturer"/>
			<form:errors path="manufacturer"/>
		</div>
		<div>
			<label for="year">Ano</label>
			<form:input path="year"/>
			<form:errors path="year"/>
		</div>

		<div>
			<label for="photo">Foto</label>
			<input type="file" name="photo"/>
			<form:errors path="photoPath"/>			
		</div>
		<div>
			<input type="submit" value="Salvar">
		</div>
	</form:form>
</body>
</html>