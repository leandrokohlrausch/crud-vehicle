<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista Ve�culos</title>
</head>
<body>

	<div>
		${message}
	</div>

	<div>
		<a href="${pageContext.request.contextPath}/vehicles/create"> Novo Ve�culo </a>
		<a href="${pageContext.request.contextPath}/users/logout"> Logout </a>
	</div>	
	<table>
		<tr>
			<td>Modelo</td>
			<td>Fabricante</td>
			<td>Ano</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<c:forEach items="${vehicles}" var="vehicle">
			<tr>
				<td><c:out value="${vehicle.getModel()}"/></td>
				<td><c:out value="${vehicle.getManufacturer()}"/></td>
				<td><c:out value="${vehicle.getYear()}"/></td>
				<td><a href="${pageContext.request.contextPath}/vehicles/show/${vehicle.getId()}">Visualizar</a> </td>
				<td><a href="${pageContext.request.contextPath}/vehicles/edit/${vehicle.getId()}">Editar</a></td>
				<td><a href="${pageContext.request.contextPath}/vehicles/delete/${vehicle.getId()}">Excluir</a></td>
				<td></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>