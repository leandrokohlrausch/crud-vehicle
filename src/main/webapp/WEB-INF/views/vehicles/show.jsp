<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Visualizar Ve�culos</title>
</head>
<body>	

	<a href="${pageContext.request.contextPath}/vehicles/"> Listagem de Ve�culos</a>
	<a href="${pageContext.request.contextPath}/vehicles/create"> Novo Ve�culo </a>
	<a href="${pageContext.request.contextPath}/users/logout"> Logout </a>

	<form:form action="${pageContext.request.contextPath}/vehicles/" method="GET" commandName="vehicle">
		
		<img src="${vehicle.getEncodeBase64Photo()}" alt="Vehicle Photo" height="100" width="100"/>
		
		<div>
			<label for="model">Modelo</label>
			<form:input path="model" readonly="readonly"/>
		</div>
		<div>
			<label for="manufacturer">Fabricante</label>
			<form:input path="manufacturer" readonly="readonly"/>
		</div>
		<div>
			<label for="year">Ano</label>
			<form:input path="year" readonly="readonly"/>
		</div>
	</form:form>
</body>
</html>