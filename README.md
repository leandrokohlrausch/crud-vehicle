Gradle 2.9
Java JDK 1.6
DB: Postgresql 9.3


Acesso ao banco de dados na aplicação. 
Alterar os seguintes valores das propriedades no arquivo /src/resources/application.properties

db.url=jdbc:postgresql://${ip_banco}:{porta_servico}/arezzo-vehicles-crud
db.username= ${usuario_do_banco}
db.password=${senha_usuario}

Rodar Script:

CREATE DATABASE "arezzo-vehicles-crud" ENCODING = 'UTF8';

CREATE TABLE vehicles (
  id SERIAL,
  model VARCHAR,
  manufacturer VARCHAR,
  year INTEGER,
  photo_path VARCHAR,
  CONSTRAINT vehicles_pk PRIMARY KEY (id)
);
	  
CREATE TABLE users (
  id SERIAL,  
  username VARCHAR,
  email VARCHAR, 
  password VARCHAR,  
  CONSTRAINT users_pk PRIMARY KEY (id)
); 

INSERT INTO users (username, email, password) VALUES ('leandro', 'leandro@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'); -- password = 12345
INSERT INTO users (username, email, password) VALUES ('rodrigo', 'rodrigo@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');
INSERT INTO users (username, email, password) VALUES ('ricardo', 'ricardo@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');
